# LibreMiami Search

[LibreMiami](https://libremiami.org) fork of searx

[See it in action](search.libremiami.org)

Repo currently hosted on [disroot git](https://git.disroot.org/LibreMiami/libremiami-search)

# Dev

## Dependencies

You need npm, see [our tutorial](https://libremiami.org/how-to-install-npm-and-node/) if you need help.

You need pip and virtualenv
```
sudo apt install python3-pip # on debian, trisquel
pip install virtualenv
```

Install node dependencies
```
make node.env
```

Then you can then prepare the themes and run the dev environment as needed 
```
make themes
make run
```
